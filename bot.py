import discord
from discord.ext import commands
import sqlite3
from sqlite3 import Error
import requests
import json
import random
connection = sqlite3.connect("score.sqlite")

TOKEN = ('')
bot = commands.Bot(command_prefix='*')

@bot.event
async def on_ready():
    print("ready for spying")
    
def hook(ct):
    url = ""
    embed = {
    "content": f"{ct}"
    }
    result = requests.post(url, data=json.dumps(embed),
                            headers={"Content-Type": "application/json"})
    
def insertdb(identification, username, score):
    with connection:
        sql = 'INSERT INTO scores (id, username, score) values(?, ?, ?)'
        
        data = [
            (str(identification), str(username), str(score)),
        ]
        connection.executemany(sql, data)
        
        out = (f":yellow_square: ```Insertion into scores with id {identification} username {username} and score {score}```")
        print(out)
        hook(out)
    
def updatedb(identification, username, score, operation):
    with connection:
        data = connection.execute('''UPDATE scores SET score = ? WHERE id = ?''', (score, identification))
        out = (f":yellow_square: ```Updated score for {username} ({identification}) \nNew score {score} [Operation type: {operation}]```")
        print(out)
        hook(out)
        
def checkgoodscores(string):
    term = string.lower()
    score = 0
    with open("goodterms.txt") as f:
        for line in f:
            if term in line.lower():
                q = (random.randint(10, 200))
                score += q
    return score

def checkbadscores(string):
    term = string.lower()
    score2 = 0
    with open("badterms.txt") as f:
        for line in f:
            if term in line.lower():
                q = (random.randint(10, 200))
                score2 += q
    return score2

def getscore(id):
    with connection:
        data = connection.execute(f"SELECT * FROM scores WHERE id LIKE '{id}' COLLATE NOCASE")
        for row in data:
            return (row[2])



@bot.event
async def on_message(message):
    term = message.content
    identification = (message.author.id)
    username = (f"{message.author.name}#{message.author.discriminator}")
    
    with connection:
        cursor = connection.execute("SELECT count(*) FROM scores WHERE id = ?", (identification,))
        data=cursor.fetchone()[0]
        
        
        if data==0:
            insertdb(identification, username, "1989")
            out = (f":green_square: ```Added user to the database [{username}]```")
            print(out)
            hook(out)
            a = checkgoodscores(term)

            if a > 0:
                c = getscore(identification)
                d = c + a
                if d > 5000:
                    return
                updatedb(identification, username, d, "Addition")
                
            b = checkbadscores(term)

            if b > 0:
                c = getscore(identification)

                d = c - a
                if d > 5000:
                    return
                updatedb(identification, username, d, "Subtraction")
            return
                
        else:
            a = checkgoodscores(term)

            if a > 0:
                c = getscore(identification)

                d = c + a
                if d > 5000:
                    return
                updatedb(identification, username, d, "Addition")
                
            b = checkbadscores(term)
            if b > 0:
                c = getscore(identification)

                d = c - b
                if d > 5000:
                    return
                updatedb(identification, username, d, "Subtraction")
            return
    
    
bot.run(TOKEN)

    
    
    